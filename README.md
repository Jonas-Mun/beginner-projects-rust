# Projects to do

- [x] factorial
- [x] 99 Bottles
- [x] Armstrong Number
- [x] Magic 8 Ball
- [x] Pythagorean Triplet checker
- [x] Rock Paper Scissors
   -> [x] Give the player the option to play again
   -> [x] Keep a record of the score
