pub mod factorial {
    use std::io;

    pub fn factorial_number() {
        println!("Please enter a number");

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).expect("Error reading from user");

        let value = buffer.trim().parse().expect("Invalid input");
        println!("{}", (1..=value).rev().product::<i32>());
    }
}
