#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub mod rock_paper_scissors {
    use std::io::{self, Read};
    enum RockPaperScissor {
        Rock,
        Paper,
        Scissor,
    }

    // Record winners
    struct GameRecord {
        player: u32,
        ai: u32,
    }

    enum Winner {
        Tie,
        User,
        AI,
    }

    pub fn run() {
        let mut game_record = GameRecord { player: 0, ai: 0};

        introduction();
        loop {
            guide();
            let input = input_handler();
            
            
            // Displays game or erros
            match input {
                Ok(play) => { 
                    let winner = play_game(play); 
                    match winner {
                        Winner::User => { 
                            game_record.player += 1;
                            println!("You Win!");
                        }
                        Winner::AI => { 
                            game_record.ai += 1;
                            println!("You Lose");
                        }
                        Winner::Tie => println!("TIE!"),
                    }
                }
                Err(err) => { println!("ERROR: {}", err); continue },
            }

            display_score(&game_record);

            let shall_continue = continue_to_play();
            if shall_continue == false{
                break;
            }
        }
    }

    fn introduction() {
        println!("Welcome to Rock Paper Scissors");
    }

    fn guide() {
        println!("Please enter either of the following:");
        println!("'rock'   'paper'    'scissors'");
    }

    fn display_score(record: &GameRecord) {
        println!("User: {}, AI: {}", record.player, record.ai);
    }

    fn continue_to_play() -> bool {
        loop {
            println!("Continue? [Y/n]");
            let mut buffer = String::new();
            io::stdin().read_line(&mut buffer).unwrap();
            let buffer = buffer.trim_end_matches('\n');

            match buffer {
                "y" | "Y" => return true,
                "n" | "N" => return false,
                _ => println!("Please try again"),
            }
        }
    }

    fn input_handler() -> Result<RockPaperScissor, String> { 
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        let buffer = buffer.trim_end_matches('\n');

        match buffer {
            "rock" => Ok(RockPaperScissor::Rock),
            "paper" => Ok(RockPaperScissor::Paper),
            "scissors" => Ok(RockPaperScissor::Scissor),
            _ => Err(String::from("Invalid input")),
            
        }
    }

    fn play_game(user_play: RockPaperScissor) -> Winner{
        let ai_answer = ai_brain();

        game_logic(user_play, ai_answer)

    }

    fn ai_brain() -> RockPaperScissor {
        let possible_answer = ["rock", "paper", "scissors"];
        let answer = possible_answer[0];    // Random

        match answer {
            "rock" => RockPaperScissor::Rock,
            "paper" => RockPaperScissor::Paper,
            "scissor" => RockPaperScissor::Scissor,
            _ => RockPaperScissor::Rock,
        }
    }

    fn game_logic(user_play: RockPaperScissor, ai_play: RockPaperScissor) -> Winner {
        // Player - (+)
        // AI     - (-)
        // 0 -> Rock
        // 1 -> Paper
        // 2 -> Scissors
        //
        // **************
        //  Player       AI
        //  Rock    -   Rock   => 0
        //  Paper   -   Paper  => 0
        //  Scissors -  Scissors => 0
        //
        //  Rock     -  Paper    => -1    AI Wins
        //  Paper    -  Rock     => 1     Player Wins
        //
        //  Rock     -  Scissors => -2    Player Wins
        //  Scissors -  Rock     => 2     AI Wins
        //
        //  Paper    -  Scissors => -1    AI Wins
        //  Scissors -  Paper    => 1     Player Wins
        let user_num = user_play as i8;    
        let ai_num = ai_play as i8;

        let result = user_num - ai_num;

        match result {
            0 => Winner::Tie,
            -1 => Winner::AI,
            1 => Winner::User,
            -2 => Winner::User,
            2 => Winner::AI,
            _ => Winner::Tie,
        }
    }
}
