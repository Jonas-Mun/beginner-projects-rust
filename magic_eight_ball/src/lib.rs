#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub mod magic_ball {
    use std::io;
    use std::io::{Write, Read, BufRead, BufReader};
    use std::{thread, time};
    use std::fs::File;
    use std::path::Path;
    use rand::Rng;
    use conrod_core;

    pub mod gui {
        use druid::widget::{Button, Label, TextBox};
        use druid::{Data, Env, Lens, Widget, WidgetExt, WindowDesc};

        #[derive(Clone, Data, Lens)]
        struct MagicState {
            question: String,
            answer: String,
        }

        pub fn run_gui(resp_path: &str) {
            /*
            let main_window = WindowDesc::new(build_gui)
                .title("Magic Eight Ball")
                .window_size((400,400));

            // create initial app state
            let initial_state = MagicState {
                question: String::from(""),
                answer: String::from(""),
                responses: prepare_responses(resp_path),
            }

            // start application
            AppLauncher::with_window(main_window)
                .launch(initial_state)
                .expect("Failed to launch application");

                */
        }

        /*
        fn build_gui() -> impl Widget<MagicState> {
            let label = Label::new(|data: &MagicState, _env: &Env| {
                data.answer
            });

            let textbox = TextBox::new()
                .with_placeholder("What do you seek?")
                .fix_width(200.0),
                lens(MagicState::question);

            let ask_button = Button::new("Ask")
                .on_click(|_event, data: &mut MagicState, _env| {
                    data.answer = retrieve_reponses(&data.responses);
                    data.question = String::from("");
                });
            let layout = Flex::column()
                .with_child(label)
                .with_child(textbox)
                .with_child(ask_button);
            
        }
        */
    }

    pub fn run(resp_path: &str) {
        let responses: Vec<String> = prepare_responses(resp_path);
        loop {
            // Input
            println!("What is your concern?");
            input_handler();

            // Loading
            thinking_prompt();

            // Show random response
            println!("{}", retrieve_response(&responses));
            break;
        }
        
    }

    fn input_handler() -> String {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        let answer = String::from("Input from user");

        buffer
    }

    fn thinking_prompt() {
        let time = 2000;
        let dot: char = '.';
        let max_dots: u32 = 3;

        let prompt = "Thinking";

        for x in 0..5 {
            let prompt_display = build_thinking_prompt(prompt, x);
            print!("{}", prompt_display);
            io::stdout().flush().unwrap();
            thread::sleep( time::Duration::from_millis(time));
            print!("\r");
            clear_line(prompt_display.len());
            //print!("\x1B[2J");    clears terminal screen
        }
    }

    fn clear_line(length: usize) {
        for x in 0..length {
            print!(" ");
        }
        io::stdout().flush().unwrap();
        print!("\r");
    }

    fn build_thinking_prompt(prompt: &str, amount: u32) -> String {
        let mut prompt_display = String::from(prompt);
        for x in 0..(amount%3) {
            prompt_display.push('.');
        }

        prompt_display
    }

    

    fn prepare_responses(file_path: &str) -> Vec<String> {
        let mut storage = Vec::new();
        
        let f = File::open(file_path).unwrap();
        let mut file = BufReader::new(&f);
        for line in file.lines() {
            let l = line.unwrap();
            storage.push(l);
        }

        storage
    }

    fn retrieve_response(resps: &Vec<String>) -> &str {
        let mut rng = rand::thread_rng();        
        let random_index = rng.gen_range(0, resps.len()); 
        &(resps[random_index])[..]
    }
}


