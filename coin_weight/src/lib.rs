#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub mod coin_weight {
    use std::collections::HashMap;
    struct CoinData {
        name: Coin,
        value: u32,
        weight: f32,
        max_per_wrapper: u32,
    }

    // Data that is obtained from the user
    // and used throughout the software.
    struct InputData {
        coin: Coin,
        total_weight: f32,
    }

    struct OutputData {
        total_wrappers: u32,
        total_value: f32,
    }

    #[derive(PartialEq, Eq, Hash)]
    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter,
    }

    enum Command {
        Exit,
        Proceed(InputData),
        TryAgain(String),
    }

    pub fn run() {
        // 0. Setup databse
        let mut database = create_database();

        // 1. Ask Input
        let input = input_handler();

        // 2. proceed with data
        match input {
            Command::Exit => println!("Exiting program"),
            Command::Proceed(data) => {
                let output_data = calculate_wraps_total_value(data.coin, data.total_weight, &database);
                println!("Total Wrappers: {}, Total value: {}", output_data.total_wrappers, output_data.total_value);
                println!("");   // new line
            }
            Command::TryAgain(msg) => println!("Entered something wrong"),
        }
    }

    fn create_database() -> HashMap<Coin, CoinData> {
        let mut database = HashMap::new();
        database.insert(Coin::Penny, create_coin(Coin::Penny, 2.50, 50));
        database.insert(Coin::Nickel, create_coin(Coin::Nickel, 5.00, 40));
        database.insert(Coin::Dime, create_coin(Coin::Dime, 2.268, 50));
        database.insert(Coin::Quarter, create_coin(Coin::Quarter, 5.67, 40));

        database
    }

    fn calculate_wraps_total_value(coin: Coin, total_weight: f32, database: &HashMap<Coin, CoinData>) -> OutputData {
        let coin_data = database(coin).unwrap(); 

        let amount_of_coins = total_weight / coin_data.weight;

        let total_wrappers: u32 = (amount_of_coins / coin_data.max_per_wrapper) + 1;  // add 1 to record one bag.
        let total_value = coin_data.value * amount_of_coins;

        OutputData { total_wrappers, total_value }

    }

    fn create_coin(coin: Coin, weight: f32, per_wrapper: u32) -> CoinData {
        CoinData { name: coin, weight, max_per_wrapper: per_wrapper }
    }
    
}
