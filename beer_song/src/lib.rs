
pub mod bottles {
    pub fn bottles_of_beer(x: i32) {
        for i in (0..=x).rev() {
            print_beers(i);
            println!("");
        }
    }

    fn print_beers(beers: i32) {
        print_first_sentence(beers);
        let next_beers = get_next_beers(beers);
        print_second_sentence(beers, next_beers);
    }

    fn print_first_sentence(beer: i32) {
        // acquire bottle plural/singular
        let mut bottles = String::from("bottle");
        if beer != 1 {
            bottles.push('s');
        }

        // build sentence
        let bottle_amount;
        if beer == 0 {
            bottle_amount = String::from("No more");
        } else {
            bottle_amount = beer.to_string();
        }

        println!("{} {} of beer on the wall, {} {} of beer",
                 bottle_amount, bottles, bottle_amount.to_lowercase(), bottles);
    }

    fn print_second_sentence(prev_beer: i32,  beer: i32) {
        let first_section;

        if beer > prev_beer {
            first_section = String::from("Go to the store and buy some more,");
        } else {
            first_section = String::from("Take one down and pass it around");
        }

        let mut bottle_noun = String::from("bottle");
        if beer != 1 {
            bottle_noun.push('s');
        }

        let bottle_amount;
        if beer == 0 {
            bottle_amount = String::from("no more");
        } else {
            bottle_amount = beer.to_string();
        }

        println!("{} {} {} of beer on the wall",
                 first_section, bottle_amount, bottle_noun);
    }

    fn get_next_beers(beers: i32) -> i32 {
        if beers == 0 {
            return 99;
        }

        (beers-1)
    }
}

