///The logic flow:
/// String -> Command
/// String -> ValidatedString
/// ValidatedString -> (a, b, c)
/// (a, b, c) -> bool
///

pub mod pyth_fact {
    use std::io::{self, Read};

    enum Command {
        Proceed(String),
        Exit,
    }

    enum ValidatedString {
        Valid(Vec<String>),
        Invalid(String),
    }

    pub fn run() {
        introduction_msg();
        loop {
            prompt();
            let input = input_handler();
            match command_input(input) {
                Ok(Command::Exit) => break,
                Ok(Command::Proceed(msg)) => {
                    match validate_string(msg) {
                        ValidatedString::Valid(valid) => {
                            let (a, b, c) = parse_string(valid); 
                            println!("{}", pythagorean_triplet(a, b, c));
                        },
                        ValidatedString::Invalid(invalid) => {
                            println!("Invalid string: {}", invalid)
                        },
                    }
                },
                Err(err) => println!("{}", err),
            }
        }
        
    }

    fn introduction_msg() {
        println!("Welcome to Pythagorean Triplets!");
    }

    fn prompt() {
        println!("Please enter three numbers as so: 'x y z'");
    }

    fn validate_string(msg: String) -> ValidatedString {
        let values: Vec<&str> = msg.split(' ').collect();

        if let Err(msg) = valid_length(&values) {
            return ValidatedString::Invalid(msg);
        }


        // validate input 
        for val in &values {
            if let Err(err) = val.parse::<u32>() {
                return ValidatedString::Invalid(String::from(*val));
            }
        }

        let values = values.iter().map(|x| { String::from(*x) }).collect();

        ValidatedString::Valid(values)

    }

    fn parse_string(val_str: Vec<String>) -> (u32, u32, u32) {
         let value_parsed: Vec<u32>  = val_str.iter().map(|x| x.parse::<u32>().unwrap()).collect();

         (value_parsed[0], value_parsed[1], value_parsed[2])
    }

    fn command_input(cmd: String) -> Result<Command, String> {

        // check commands
        match &cmd[..] {
            "exit" => { 
                println!("We are exiting!");
                Ok(Command::Exit)
            },
            _ => { 
                Ok(Command::Proceed(cmd))
            }, 
        }
    }

    fn input_handler() -> String {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        let buffer = buffer.trim_end_matches('\n');       

        String::from(buffer)
    }

    fn valid_length(buffer: &Vec<&str>) -> Result<bool, String> {
        if buffer.len() < 3 {
            return Err(String::from("ERROR: You entered less than 3 values"));
        }
        if buffer.len() > (3+1) {
            return Err(String::from("ERROR: You entered more than 3 values"));
        }
        Ok(true)

    }

    pub fn pythagorean_triplet(a: u32, b: u32, c: u32) -> bool {
        let mut divisor: u32 = 2;

        while divisor <= a && divisor <= b && divisor <= c {


            if common_divisor(a, b, divisor) || common_divisor(a, c, divisor) || common_divisor(b, c, divisor) {
                return false;
            }
            divisor = divisor + 1;
        }
        true
    }

    fn common_divisor(a: u32, b: u32, div: u32) -> bool {
        let a_mod = a % div;
        let b_mod = b % div;

        if a_mod == 0 && b_mod == 0{
            return true; 
        }
        false
    }
}

